package senjicorp.gok.hexgame.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import senjicorp.gok.hexgame.dao.UserDaoImpl;
import senjicorp.gok.hexgame.dao.mapper.UserMapper;

@Configuration
public class PersistenceConfig {

	@Bean
	public UserDaoImpl getUserDaoImpl() {
		return new UserDaoImpl();
	}

	@Bean
	public UserMapper getUserMapper() {
		return new UserMapper();
	}

}
