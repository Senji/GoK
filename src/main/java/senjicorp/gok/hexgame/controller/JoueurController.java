package senjicorp.gok.hexgame.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import senjicorp.gok.hexgame.dao.UserDaoImpl;

/**
 * Handles requests for the application home page.
 */
@Controller
public class JoueurController {

	private static final Logger logger = LoggerFactory.getLogger(JoueurController.class);

	@Autowired
	protected UserDaoImpl userDao;

	@RequestMapping(value = "/joueur", method = RequestMethod.GET)
	public ModelAndView joueurPage() {
		logger.info("Welcome joueur!");

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Salutations joueur");
		model.addObject("message", "This is protected page!");
		model.setViewName("admin");

		return model;

	}

	@RequestMapping(value = "/unite", method = RequestMethod.GET)
	public ModelAndView unitePage() {
		logger.info("Cr�ation unit�!");
		ModelAndView model = new ModelAndView();

		return model;

	}

}
