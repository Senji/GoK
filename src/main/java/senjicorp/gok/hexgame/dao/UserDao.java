package senjicorp.gok.hexgame.dao;

import java.util.List;

import senjicorp.gok.hexgame.model.User;

public interface UserDao {

	User findByName(String name);

	List<User> findAll();

}
