package senjicorp.gok.hexgame.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import senjicorp.gok.hexgame.dao.mapper.UserMapper;
import senjicorp.gok.hexgame.model.User;

public class UserDaoImpl implements UserDao {

	UserMapper data = new UserMapper();

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.data.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public User findByName(String name) {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("name", name);

		String sql = "SELECT * FROM users WHERE name=:name";

		User result = data.namedParameterJdbcTemplate.queryForObject(sql, params, new UserMapper());

		// new BeanPropertyRowMapper(Customer.class));

		return result;

	}

	@Override
	public List<User> findAll() {

		Map<String, Object> params = new HashMap<String, Object>();

		String sql = "SELECT * FROM users";

		List<User> result = data.namedParameterJdbcTemplate.query(sql, params, new UserMapper());

		return result;

	}

}
