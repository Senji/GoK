/* global angular:false */
'use strict';

var app = angular.module('uniteApp', ['uniteApp.DTO', 'uniteApp.controller']);