/* global angular:false */
'use strict';

/**
 * Controller pour la gestion du panier.
 */
// S'isoler a l'aide d'une fonction anonyme auto executable pour ne pas poluer le contexte global
(function() {

    // Declarer les controleurs en tant que fonction javascript standard pour favoriser
    // la completion dans les ide et simplifier la comprehension du code javascript
    var myCtrl = function($scope, $log, UniteDTO) {
        $scope.unite = new UniteDTO();
        $scope.titre = "Création d'une unité";
        $scope.typeAccessoire = undefined;
        $scope.ajoutAccessoire = false;

        $scope.erreurTaille = undefined;

        $scope.incrementeTaille = function() {
            $log.debug('Augmentation taille');
            $scope.unite.taille++;
            majUnite();
        }

        $scope.decrementeTaille = function() {
            $log.debug('Diminution taille');
            if ($scope.unite.taille > 0) {
                $scope.unite.taille--;
            }
            majUnite();
        }

        $scope.modifierTaille = function() {
            $log.debug('modification de la taille : ' + $scope.unite.taille);
            if ($scope.unite.taille > 0 && $scope.unite.taille <= 200) {
                $scope.erreurTaille = undefined;
                majUnite();
            } else {
                $scope.erreurTaille = "La taille d'une unité doit être comprise entre 1 et 200";
            }
        }

        $scope.chargeListeAccessoire = function() {
            $log.debug("Chargement de la liste des accessoires : $scope.typeAccessoire = " + $scope.typeAccessoire);
            $scope.listeTypeAccessoire = $scope.unite.accessoiresPossible[$scope.typeAccessoire];
        }

        $scope.ajouterAccessoire = function(typeAccessoire, accessoire) {
            $log.debug("Ajout d'un accessoire à l'unité : " + typeAccessoire + ", " + accessoire);
            $scope.unite.ajoutAccessoire(typeAccessoire, accessoire);
        }

        $scope.aAccessoire = function(typeAccessoire, accessoire) {
            //$log.debug("Vérification si l'unité à l'accessoire : " + typeAccessoire + ", " + accessoire);
            return $scope.unite.aAccessoire(typeAccessoire, accessoire);
        }

        $scope.accessoirePossible = function(typeAccessoire, accessoire) {
            //$log.debug("Vérification si l'unité à droit à l'accessoire : " + typeAccessoire + ", " + accessoire);
            return $scope.unite.accessoirePossible(typeAccessoire, accessoire)
        }

        $scope.retirerAccessoire = function(typeAccessoire, accessoire) {
            $log.debug("Retrait d'un accessoire à l'unité : " + typeAccessoire + ", " + accessoire);
            $scope.unite.retraitAccessoire(typeAccessoire, accessoire);
        }

        $scope.majExperience = function() {
            $log.debug("Expérience modifiée");
            majUnite();
        }

        var majUnite = function () {
            if ($scope.erreurTaille !== undefined);
             $scope.unite.majUnite();
        }

        // Mise à jour initiale de l'unité
        majUnite();

    };


    // Definir explicitement les dependances nécessaires au controleur pour etre insensible a la minification
    myCtrl.$inject = ['$scope', '$log', 'UniteDTO'];

    // Recuperer un module a l'aide de son nom sans poluer le contexte global de variables inutiles
    angular.module('uniteApp.controller').controller('creationController', myCtrl);
})();