/* global angular:false */
'use strict';

// Définition d'un objet personne
angular.module('uniteApp.DTO').factory('UniteDTO', ['$log',
    function($log) {

        // Initialisation de la personne
        function UniteDTO() {

            this.nom = "";
            this.type = "Infanterie";
            this.taille = 100;
            this.poids = 100;
            this.vitesse = 2;
            this.points = 3;
            // Liste des accessoires (pour un affichage plus simple et rapide)
            this.listeAccessoires = [];
            this.experience = "Bleue";

            // Liste des accessoires plus fonctionnelles (pour la gestion des possibilitées et autres)
            this.accessoires = {
                "monture": [],
                "armeContact": [],
                "armeDistance": [],
                "armure": [],
                "bouclier": [],
                "soutien": []
            };

            this.typeAccessoiresPossibles = ["monture", "armeContact", "armeDistance", "armure", "bouclier", "soutien"];

            this.accessoiresPossible = {
                "monture": ["cheval", "char"],
                "armeContact": ["épée", "hache", "lance", "dague"],
                "armeDistance": ["arc", "arbalète", "fronde", "javeline"],
                "armure": ["matelassée", "cuir", "maille", "écaille", "harnois"],
                "bouclier": ["targe", "écu", "pavois"],
                "soutien": ["catapulte", "baliste", "bélier"]
            };

            this.experiencePossible = ["Bleue", "Entraînée", "Vétéran", "Elite"];
        }

        UniteDTO.prototype = {
            ajoutAccessoire: function(typeAccessoire, accessoire) {
                if (this.accessoires[typeAccessoire].indexOf(accessoire) === -1) {
                    this.accessoires[typeAccessoire].push(accessoire);
                    this.listeAccessoires.push(accessoire);
                    $log.debug("accessoire ajouté");
                    this.majUnite();
                } else {
                    $log.debug("accessoire déjà présent");
                }
            },

            retraitAccessoire: function(typeAccessoire, accessoire) {
                var index = this.accessoires[typeAccessoire].indexOf(accessoire);
                if (index != -1) {
                    //suppression d'un élément à l'index donné
                    this.accessoires[typeAccessoire].splice(index, 1);

                    index = this.listeAccessoires.indexOf(accessoire);
                    //suppression d'un élément à l'index donné
                    this.listeAccessoires.splice(index, 1);
                    $log.debug("accessoire retiré");
                    this.majUnite();
                } else {
                    $log.debug("accessoire non présent");
                }
            },

            aAccessoire: function(typeAccessoire, accessoire) {
                if (this.accessoires[typeAccessoire].indexOf(accessoire) === -1) {
                    return false;
                } else {
                    return true;
                }
            },

            accessoirePossible: function(typeAccessoire, accessoire) {
                // gestion fine par accessoires
                switch (accessoire) {
                    case 'dague':
                        // Pas de dague pour la cavalerie
                        if (this.type === "Cavalerie") {
                            return false;
                        }
                        break;
                    case 'pavois':
                        // Pas de pavois pour la cavalerie
                        if (this.type === "Cavalerie") {
                            return false
                        }
                        // Pas de pavois pour les archers
                        if (this.accessoires['armeDistance'].indexOf('arc') !== -1) {
                            return false;
                        }
                        break;
                    case 'écu':
                        // Pas d'écu pour les archer ou les arbaletriers
                        if (this.accessoires['armeDistance'].indexOf('arc') !== -1) {
                            return false;
                        }
                        if (this.accessoires['armeDistance'].indexOf('arbalète') !== -1) {
                            return false;
                        }
                        break;
                }
                // gestion grosse par type d'accessoires
                switch (typeAccessoire) {
                    case 'monture':
                        if (this.type === "Siège") {
                            return false;
                        }
                        // Pas plus d'une monture
                        if (this.accessoires[typeAccessoire].length !== 0 && this.accessoires[typeAccessoire][0] !== accessoire) {
                            return false;
                        }
                        break;
                    case 'armeContact':
                        // Pas plus d'une arme de corps à corps
                        if (this.accessoires[typeAccessoire].length !== 0 && this.accessoires[typeAccessoire][0] !== accessoire) {
                            return false;
                        }
                        break;
                    case 'armeDistance':
                        // Pas d'arme à distance pour les unités de siège
                        if (this.type === "Siège") {
                            return false;
                        }
                        // Pas plus d'une arme à distance
                        if (this.accessoires[typeAccessoire].length !== 0 && this.accessoires[typeAccessoire][0] !== accessoire) {
                            return false;
                        }
                        break;
                    case 'armure':
                        // Pas plus de 2 armure
                        if (this.accessoires[typeAccessoire].length >= 2 && this.accessoires[typeAccessoire].indexOf(accessoire) === -1) {
                            return false;
                        }
                        if (this.accessoires[typeAccessoire].length === 1 && this.accessoires[typeAccessoire][0] !== 'matelassée' && this.accessoires[typeAccessoire][0] !== accessoire) {
                            return false;
                        }
                        break;
                    case 'bouclier':
                        // Pas de bouclier pour les unités de siège
                        if (this.type === "Siège") {
                            return false;
                        }
                        // Pas plus d'un bouclier
                        if (this.accessoires[typeAccessoire].length !== 0 && this.accessoires[typeAccessoire][0] !== accessoire) {
                            return false;
                        }
                        break;
                    case 'soutien':
                        // Pas d'arme de siège pour la cavalerie
                        if (this.type === "Cavalerie") {
                            return false;
                        }
                        // Pas d'arme de siège si armes à distance
                        if (this.accessoires['armeDistance'].length !== 0) {
                            return false;
                        }
                        // Une seule arme de siège
                        if (this.type === "Siège" && this.accessoires[typeAccessoire][0] !== accessoire) {
                            return false;
                        }
                        break;
                }
                return true;
            },

            majUnite: function() {
                $log.debug("mise à jour de l'unité");
                var poids = this.taille;
                var type = 'Infanterie';
                var vitesse = 2;
                var points = 3;
                var accessoire;

                var typeAccessoire = 'monture';
                var listeDuType = this.accessoires[typeAccessoire];
                for (accessoire in listeDuType) {
                    switch (listeDuType[accessoire]) {
                        case 'cheval':
                            poids = poids * 1.5;
                            type = 'Cavalerie';
                            vitesse = 6;
                            points += 4;
                            break;
                        case 'char':
                            poids = poids * 1.75;
                            type = 'Cavalerie';
                            vitesse = 4;
                            points += 3;
                            break;
                    }
                }

                typeAccessoire = 'armeContact';
                listeDuType = this.accessoires[typeAccessoire];
                for (accessoire in listeDuType) {
                    switch (listeDuType[accessoire]) {
                        case 'épée':
                            poids = poids * 1.05;
                            break;
                        case 'hache':
                            poids = poids * 1.06;
                            break;
                        case 'lance':
                            poids = poids * 1.08;
                            break;
                        case 'dague':
                            poids = poids * 1.02;
                            break;
                    }
                }
                if (listeDuType.length > 0) {
                    points++;
                }

                typeAccessoire = 'armeDistance';
                listeDuType = this.accessoires[typeAccessoire];
                for (accessoire in listeDuType) {
                    switch (listeDuType[accessoire]) {
                        case 'arc':
                            poids = poids * 1.07;
                            break;
                        case 'arbalète':
                            poids = poids * 1.10;
                            break;
                        case 'fronde':
                            poids = poids * 1.03;
                            break;
                        case 'javeline':
                            poids = poids * 1.10;
                            break;
                    }
                }
                if (listeDuType.length > 0) {
                    points++;
                }

                typeAccessoire = 'armure';
                listeDuType = this.accessoires[typeAccessoire];
                for (accessoire in listeDuType) {
                    switch (listeDuType[accessoire]) {
                        case 'matelassée':
                            poids = poids * 1.04;
                            break;
                        case 'cuir':
                            poids = poids * 1.08;
                            break;
                        case 'maille':
                            poids = poids * 1.14;
                            break;
                        case 'écaille':
                            poids = poids * 1.2;
                            break;
                        case 'harnois':
                            poids = poids * 1.4;
                            break;
                    }
                }
                if (listeDuType.length === 2) {
                    vitesse = vitesse * 0.9;
                }
                if (listeDuType.length > 0) {
                    points += listeDuType.length;
                }

                typeAccessoire = 'bouclier';
                listeDuType = this.accessoires[typeAccessoire];
                for (accessoire in listeDuType) {
                    switch (listeDuType[accessoire]) {
                        case 'targe':
                            poids = poids * 1.01;
                            break;
                        case 'écu':
                            poids = poids * 1.03;
                            break;
                        case 'pavois':
                            poids = poids * 1.1;
                            break;
                    }
                }
                if (listeDuType.length > 0) {
                    points++;
                }

                typeAccessoire = 'soutien';
                listeDuType = this.accessoires[typeAccessoire];
                if (listeDuType.length !== 0) {
                    poids = poids * 2;
                    type = 'Siège';
                    vitesse = vitesse * 0.5;
                    points += 8
                }

                // Mise à jour des points
                switch (this.experience) {
                    case "Bleue":
                        points = points * 0.75;
                        vitesse = vitesse * 0.8;
                        break;
                    case "Entraînée":
                        points = points * 1;
                        break;
                    case "Vétéran":
                        points = points * 1.5;
                        break;
                    case "Elite":
                        points = points * 2.25;
                        vitesse = vitesse * 1.2;
                        break;
                }

                this.poids = poids;
                this.type = type;
                this.vitesse = vitesse;
                this.points = points;
            }
        };

        return (UniteDTO);

    }
]);