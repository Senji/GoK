<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page pageEncoding="UTF-8" session="false"%>
<html>
	<head>
		<meta charset="UTF-8">

		<title>Unite</title>

		<link rel="stylesheet" type="text/css" href="resources/css/root.css">
		<link rel="stylesheet" type="text/css" href="resources/css/unite.css">


		<script src="resources/js/lib/angularjs_1.5.8.js"></script>

		<script src="resources/js/app/unite/unite_modules.js"></script>
		<script src="resources/js/app/unite/dto/unite_dto.js"></script>
		<script src="resources/js/app/unite/controller/creation_controller.js"></script>
		<script src="resources/js/app/unite/unite_app.js"></script>
	</head>
	<body>
		<div ng-app="uniteApp" ng-controller="creationController">
			{{titre}}
			<br />
			<br />
			<div class="description">
				Descriptif de l'unité :<br />
				<br />
				<span class="param">nom : </span><br /><span class="value">{{unite.nom}}</span><br />
				<span class="param">type : </span><br /><span class="value">{{unite.type}}</span><br />
				<span class="param">expérience : </span><br /><span class="value">{{unite.experience}}</span><br />
				<span class="param">taille : </span><br /><span class="value">{{unite.taille}}</span><br />
				<span class="param">poids : </span><br /><span class="value">{{unite.poids}}</span><br />
				<span class="param">vitesse : </span><br /><span class="value">{{unite.vitesse}}</span><br />
				<span class="param">points : </span><br /><span class="value">{{unite.points}}</span><br />
				<span class="param">accessoires : </span><br />
				<span  class="value" ng-repeat="accessoire in unite.listeAccessoires">
					<ng-if ng-if="!$last">
						{{accessoire}},
					</ng-if>
					<ng-if ng-if="$last">
						{{accessoire}}
					</ng-if>
				</span><br />
			</div>
			<div class="menu-modif">
				Modifications de l'unité : <br />
				<br />
				<span class="param">nom : </span>
				<input type="text" maxlength="50" ng-model="unite.nom">
				<br />

				<span class="param">expérience : </span>
				<select ng-model="unite.experience" ng-change="majExperience()">
					<option ng-repeat="value in unite.experiencePossible" value="{{value}}">{{value}}</option>
				</select>
				<br />

				<span class="param">taille : </span>
				<button ng-click="incrementeTaille()" ng-if="erreurTaille === undefined">+</button>
				<input type="text" ng-model="unite.taille" ng-change="modifierTaille()">
				<button ng-click="decrementeTaille()" ng-if="erreurTaille === undefined">-</button>
				<br />
				<span class="erreur" ng-if="erreurTaille !== undefined">{{erreurTaille}}<br/></span>

				<span class="param">accessoire : </span>
				<input type="checkbox" ng-model="ajoutAccessoire">
				<br />

				<select ng-model="typeAccessoire" ng-change="chargeListeAccessoire()"  ng-show="ajoutAccessoire">
					<option ng-repeat="value in unite.typeAccessoiresPossibles" value="{{value}}">{{value}}</option>
				</select>
				<br />
			</div>
			<div class="menu-accessoire" ng-show="ajoutAccessoire">
				<div ng-repeat="accessoire in listeTypeAccessoire">
					<span ng-if="accessoirePossible(typeAccessoire, accessoire)">
						{{accessoire}} <button ng-click="ajouterAccessoire(typeAccessoire, accessoire)" ng-show="!aAccessoire(typeAccessoire, accessoire)">+</button><button ng-click="retirerAccessoire(typeAccessoire, accessoire)" ng-show="aAccessoire(typeAccessoire, accessoire)">-</button><br />
					</span>
				</div>
			</div>
		</div>
	</body>
</html>
