<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page pageEncoding="UTF-8" session="false"%>
<html>
<head>
<title>Home</title>
<link rel="stylesheet" type="text/css" href="resources/css/root.css">
<link rel="stylesheet" type="text/css" href="resources/css/hex.css">
</head>
<body>
	<h1>Salutations</h1>

	<P>The time on the server is ${serverTime}.</P>

	<br />

	<div id="content">
		<div class="hex-row">
				<a class="hex" href="<c:url value="/admin"/>">Admin</a>
				<a class="hex" href="<c:url value="/joueur"/>">Joueur</a>
				<a class="hex" href="<c:url value="/unite"/>">Unité</a>
		</div>
	</div>
</body>
</html>
